json.array!(@writters) do |writter|
  json.extract! writter, :id, :name
  json.url writter_url(writter, format: :json)
end
