json.array!(@movies) do |movie|
  json.extract! movie, :id, :name, :personable_id, :personable_type
  json.url movie_url(movie, format: :json)
end
