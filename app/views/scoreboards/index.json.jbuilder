json.array!(@scoreboards) do |scoreboard|
  json.extract! scoreboard, :id, :match_day, :status, :home_team_id, :away_team_id, :home_score, :away_score
  json.url scoreboard_url(scoreboard, format: :json)
end
