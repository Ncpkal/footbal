class Folder < ActiveRecord::Base
  belongs_to :parent_folder,-> { where isRoot: true }, class_name: Folder  
  has_many :child_folders, class_name: Folder, foreign_key: "parent_folder_id"
  has_many :super_child_folders, class_name: Folder, foreign_key: "child_folder_id"
end
