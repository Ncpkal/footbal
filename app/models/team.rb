class Team < ActiveRecord::Base
	belongs_to :user
	has_many :players, dependent: :nullify
	has_many :scoreboards

	validates :team_name, presence: true, uniqueness: true
end
