class Movie < ActiveRecord::Base
  belongs_to :personable, polymorphic: true
end
