class Ability
  include CanCan::Ability

  def initialize(user)
    if user
      if user.developer?
        can :manage, :all
      else
        cannot :manage, :all
        #start authorization from here.
        
      end
    end
  end
end
