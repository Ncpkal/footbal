class Scoreboard < ActiveRecord::Base
  belongs_to :home_team, class_name: 'Team', foreign_key: 'home_team_id'
  belongs_to :away_team, class_name: 'Team', foreign_key: 'away_team_id'
 
 
  validate :cannot_participate_for_three_days
  validate :check_status
  validate :cannot_be_today_or_earlier
  validate :cannot_play_same_month



	# def score
	# 	[home_score, away_score].join(' ')
	# end

	# def score=(score)
	# 	split = score.split(' ', 2)
	# 	self.home_score = split.first
	# 	self.away_score = split.last
	# end


	# before_save :concat_score
	# # Zenon's code. Find the correct syntax and make it work
	#  	after_find do  |scoreboard|
	#  		scoreArray = scoreboard.score.split('#')
	#  		this.home_score = scoreArray[0]
	#  		this.away_score = scoreArray[1]
	#     	puts "You have found an object!"
	#   	end
	# # Zenon's code. Find the correct syntax and make it work

	# 	def concat_score
	# 		this.score = this.home_score + '#' + this.away_score
	# 	end
	
	def cannot_participate_for_three_days
		# if match_day <= Date.today + 3.days
		if Scoreboard.where( "(home_team_id = ? OR away_team_id = ?) AND match_day > ?", home_team_id, away_team_id, match_day - 3.days).exists?
			errors[:base]<< " Team cannot participate in more than 1 match within 3 days"
		end
	end

	def check_status
  		if (status == 'Ongoing' || status == 'Completed') && (match_day != Date.today)
  			errors.add(:status, "can't be ongoing/complete when its not a match date")
  		end
  	end


	def cannot_be_today_or_earlier
		if match_day <= Date.today
			errors.add(:match_day, "cannot be today or earlier")
		end
	end

	def cannot_play_same_month
		if Scoreboard.exists?(["((home_team_id = ? AND away_team_id = ?) OR away_team_id = ? AND home_team_id = ?) AND match_day > ?", home_team_id, away_team_id, home_team_id, away_team_id, match_day - 30.days])
			errors[:base]<< " Team cannot play on same month"
		end
	end
end
