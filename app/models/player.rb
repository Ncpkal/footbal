class Player < ActiveRecord::Base
	belongs_to :team, :counter_cache => true
	 # scope :transfers, -> { where(team_id: nil) }

	 validates :name, presence: true
end
