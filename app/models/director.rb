class Director < ActiveRecord::Base
	has_one :movie, as: :personable
end
