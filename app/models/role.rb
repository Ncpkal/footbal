class Role < ActiveRecord::Base
  ADMIN=1
  NORMAL=2
  INACTIVE=3

  def self.admin; find_by_id(ADMIN); end
  def self.normal; find_by_id(NORMAL); end
  def self.inactive; find_by_id(INACTIVE); end
                
end
