class WrittersController < ApplicationController
  before_action :set_writter, only: [:show, :edit, :update, :destroy]

  # GET /writters
  # GET /writters.json
  def index
    @writters = Writter.all
  end

  # GET /writters/1
  # GET /writters/1.json
  def show
  end

  # GET /writters/new
  def new
    @writter = Writter.new
  end

  # GET /writters/1/edit
  def edit
  end

  # POST /writters
  # POST /writters.json
  def create
    @writter = Writter.new(writter_params)

    respond_to do |format|
      if @writter.save
        format.html { redirect_to @writter, notice: 'Writter was successfully created.' }
        format.json { render :show, status: :created, location: @writter }
      else
        format.html { render :new }
        format.json { render json: @writter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /writters/1
  # PATCH/PUT /writters/1.json
  def update
    respond_to do |format|
      if @writter.update(writter_params)
        format.html { redirect_to @writter, notice: 'Writter was successfully updated.' }
        format.json { render :show, status: :ok, location: @writter }
      else
        format.html { render :edit }
        format.json { render json: @writter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /writters/1
  # DELETE /writters/1.json
  def destroy
    @writter.destroy
    respond_to do |format|
      format.html { redirect_to writters_url, notice: 'Writter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_writter
      @writter = Writter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def writter_params
      params.require(:writter).permit(:name)
    end
end
