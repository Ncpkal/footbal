class TeamsController < ApplicationController
  before_action :set_team, only: [:show, :edit, :update, :destroy]
  # load_and_authorize_resource

  # GET /teams
  # GET /teams.json
  def index
    @teams = Team.all
    render json: @teams
  end

  # GET /teams/1
  # GET /teams/1.json
  def show
    render json: @team 
  end

  # GET /teams/new
  def new
    # @team = current_user.teams.build
    @team = Team.new
    render json: @team
  end

  # GET /teams/1/edit
  def edit
    # render json: @team
  end

  # POST /teams
  # POST /teams.json
  def create
    # @team = current_user.teams.build(team_params)
    @team = Team.new(team_params)
  
    respond_to do |format|
      if @team.save
        
        render :show, status: :created, location: @team 
      else
        
        render json: @team.errors, status: :unprocessable_entity
      end
    end
  end

  # PATCH/PUT /teams/1
  # PATCH/PUT /teams/1.json
  def update
    respond_to do |format|
      if @team.update(team_params)
        format.html { redirect_to @team, notice: "Team was successfully updated." }
        format.json { render :show, status: :ok, location: @team }
      else
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /teams/1
  # DELETE /teams/1.json
  def destroy
    @team.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team
      @team = Team.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_params
      params.require(:team).permit(:team_name, :founded_date, :home_city, :president, :coach)
    end
end
