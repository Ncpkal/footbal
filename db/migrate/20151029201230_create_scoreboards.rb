class CreateScoreboards < ActiveRecord::Migration
  def change
    create_table :scoreboards do |t|
      t.date :match_day
      t.string :status
      t.integer :home_team_id
      t.integer :away_team_id
      t.integer :score

      t.timestamps null: false
    end
  end
end
