class AddPlayersCountToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :players_count, :integer
  end
end
