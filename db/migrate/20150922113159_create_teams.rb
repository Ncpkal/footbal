class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :team_name
      t.date :founded_date
      t.string :home_city
      t.string :president
      t.string :coach

      t.timestamps null: false
    end
  end
end
