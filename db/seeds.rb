# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

team_list = [
	["watford", 2015, "demo", "demo", "demo", 2 ],
	["Westbrom", 2015, "demo", "demo", "demo", 3 ],
	["Swansea", 2015, "demo", "demo", "demo", 3 ],
	["Stoke", 2015, "demo", "demo", "demo", 3 ],
	
	["Chelsea", 2015, "demo", "demo", "demo", 4 ],
	["Norwich city", 2015, "demo", "demo", "demo", 4 ],
	["Bournemoth", 2015, "demo", "demo", "demo", 4 ],
	["Tottenham", 2015, "demo", "demo", "demo", 5 ],
	["Newcastle", 2015, "demo", "demo", "demo", 5 ],
	["Sunderland", 2015, "demo", "demo", "demo", 5 ]



]

team_list.each do |team_name, founded_date, home_city, president, coach, user_id|
	Team.create(team_name: team_name, founded_date: founded_date, home_city: home_city, president: president, coach: coach, user_id: user_id)
end