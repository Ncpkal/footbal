require 'test_helper'

class RoomUsersControllerTest < ActionController::TestCase
  setup do
    @room_user = room_users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:room_users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create room_user" do
    assert_difference('RoomUser.count') do
      post :create, room_user: { folder_id: @room_user.folder_id, room_id: @room_user.room_id, user_id: @room_user.user_id }
    end

    assert_redirected_to room_user_path(assigns(:room_user))
  end

  test "should show room_user" do
    get :show, id: @room_user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @room_user
    assert_response :success
  end

  test "should update room_user" do
    patch :update, id: @room_user, room_user: { folder_id: @room_user.folder_id, room_id: @room_user.room_id, user_id: @room_user.user_id }
    assert_redirected_to room_user_path(assigns(:room_user))
  end

  test "should destroy room_user" do
    assert_difference('RoomUser.count', -1) do
      delete :destroy, id: @room_user
    end

    assert_redirected_to room_users_path
  end
end
